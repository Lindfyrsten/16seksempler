package singleton;

public class Singleton_TestApp {

    public static void main(String[] args) {
        Singleton_Eksempel singleton_Eksempel = Singleton_Eksempel.getInstance();
        System.out.println(singleton_Eksempel.getValue());
        singleton_Eksempel.count();
        singleton_Eksempel.count();
        System.out.println(singleton_Eksempel.getValue());
        singleton_Eksempel.times2();
        System.out.println(singleton_Eksempel.getValue());
        Singleton_Eksempel.getInstance().count();
        System.out.println(singleton_Eksempel.getValue());
        singleton_Eksempel.zero();
        System.out.println(singleton_Eksempel.getValue());
    }

}
