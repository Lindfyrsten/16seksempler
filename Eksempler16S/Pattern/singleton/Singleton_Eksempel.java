package singleton;

public class Singleton_Eksempel {
    private int value;
    private static Singleton_Eksempel singleton_Eksempel;

    private Singleton_Eksempel() {
        value = 0;
    }

    public static Singleton_Eksempel getInstance() {
        if (singleton_Eksempel == null) {
            singleton_Eksempel = new Singleton_Eksempel();
        }
        return singleton_Eksempel;
    }
    
    public void count() {
        value++;
    }
    
    public void times2() {
        value = value * 2;
    }
    
    public void zero() {
        value = 0;
    }
    
    public int getValue() {
        return value;
    }
}
