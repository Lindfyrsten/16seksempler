package observable;

import java.util.Observable;

public class Observable_SubjectUr extends Observable {


	private int tid;

	public Observable_SubjectUr() {
		tid = 0;
	}
	
	public int getTid() {
		return tid;
	}

	public void tiktak() {
		tid++;
		setChanged();
		notifyObservers();
	}
}
