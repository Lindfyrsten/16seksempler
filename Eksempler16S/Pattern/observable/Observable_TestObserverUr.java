package observable;

import java.util.Observer;

public class Observable_TestObserverUr {
    
    public Observable_TestObserverUr() {
        super();
    }
    
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        Observable_SubjectUr ur = new Observable_SubjectUr();
        Observer analog = new Observable_AnalogtUr(ur);
        Observer digital = new Observable_DigitaltUr(ur);
        for (int i = 0; i < 4; i++) {
            ur.tiktak();
        }
    }
}
