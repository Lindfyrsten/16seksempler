package observable;

import java.util.Observable;
import java.util.Observer;

public class Observable_DigitaltUr implements Observer {
	/**
	 * ObserverDigitaltUr constructor comment.
	 */
	public Observable_DigitaltUr(Observable o) {
		o.addObserver(this);
	}
	
	public void update(Observable o, Object arg) {
		int tid = ((Observable_SubjectUr) o).getTid();
		System.out.println("Digitalt " + tid);
		
	}

}
