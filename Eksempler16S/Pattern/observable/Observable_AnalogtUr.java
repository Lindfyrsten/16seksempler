package observable;

import java.util.Observable;
import java.util.Observer;

public class Observable_AnalogtUr implements Observer {
    
    private Observable subject;
    
    public Observable_AnalogtUr(Observable o) {
        o.addObserver(this);
        subject = o;
    }
    
    @Override
    public void update(Observable o, Object arg1) {
        int tid = ((Observable_SubjectUr) o).getTid();
        System.out.println("Analogt " + tid);
    }
    
    public Observable getSubject() {
        return subject;
    }
}
