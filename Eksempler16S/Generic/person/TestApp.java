package person;
import java.util.ArrayList;
import java.util.Collections;
public class TestApp {

	public static void main(String[] args) {
		Navn n1 = new Navn("Per", "Toft");
		Navn n2 = new Navn("Albert", "Alberto");
		Navn n3 = new Navn("Qwark", "Eede");
		Person p1 = new Person<>(n1.toString());
		Person<?> p2 = new Person<>(n2);
		Person<?> p3 = new Person<>(n3);
		// System.out.println(p1);
		// System.out.println(p2);
		// System.out.println(p2.compareTo(p1));
		ArrayList<Person<?>> persons = new ArrayList<>();
		persons.add(p3);
		persons.add(p1);
		persons.add(p2);
		Collections.sort(persons);
		System.out.println(persons);
	}
}
