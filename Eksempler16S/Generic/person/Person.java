package person;
public class Person<T extends Comparable<T>> implements Comparable<Person<T>> {

	private T navn;

	Person(T navn) {
		this.navn = navn;
	}

	public T getNavn() {
		return navn;
	}

	@Override
	public int compareTo(Person<T> o) {
		return getNavn().toString().compareTo(o.getNavn().toString());
	}

	@Override
	public String toString() {
		return navn.toString();
	}
}
