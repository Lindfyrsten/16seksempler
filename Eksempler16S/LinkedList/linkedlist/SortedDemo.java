package linkedlist;

public class SortedDemo {

	public static void main(String[] args) {

		String a = "A";
		String b = "B";
		String c = "C";
		String d = "D";
		String e = "E";
		String f = "F";
		String g = "G";
		SortedLinkedList list = new SortedLinkedList();
		list.addElement(d);
		list.addElement(c);
		list.addElement(e);
		list.addElement(b);
		list.addElement(a);
		list.addElement(g);
		list.addElement(f);
		System.out.println("\nAntal: " + list.countElements());
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		System.out.println("");
		list.removeLast();
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		System.out.println("\nAntal: " + list.countElements());
		list.removeElement(a);
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		System.out.println("\nAntal: " + list.countElements());
		list.removeElement(e);
		list.removeElement(d);
		list.udskrivElements();

		SortedLinkedList list2 = new SortedLinkedList();
		list2.addElement(e);
		list2.addElement(a);
		list2.addElement(c);
		list2.addElement(d);
		list2.addElement(g);
		System.out.print("\nElementer i liste 2: ");
		list2.udskrivElements();
		list2.addAll(list);
		System.out.print("\nElementer i liste 2: ");
		list2.udskrivElements();

	}

}
