package linkedlist;

public class SortedLinkedListDouble {
	private Node first;
	private Node last;
	private int size;

	public SortedLinkedListDouble() {
		first = new Node();
		last = new Node();
		first.next = last;
		last.prev = first;
		size = 0;
	}

	/**
	 * Tilføjer e til listen, så listen fortsat er sorteret i henhold til den naturlige ordning på elementerne
	 */
	public void addElement(String e) {

		Node newNode = new Node();
		newNode.data = e;
		if (first.data == null) {
			newNode.next = newNode;
			newNode.prev = newNode;
			first = newNode;
			last = first;
		} else if (e.compareTo(first.data) <= 0) {
			newNode.prev = last;
			last.next = newNode;
			first.next = newNode;
			newNode.next = first;
			first = newNode;
		} else if (e.compareTo(last.data) >= 0) {
			last.next = newNode;
			newNode.prev = last;
			newNode.next = first;
			first.prev = newNode;
			last = newNode;
		} else {
			Node temp = first;
			Node ptr = first.next;
			boolean found = false;
			while (!found && ptr != null) {
				if (e.compareTo(temp.data) >= 0 && e.compareTo(ptr.data) <= 0) {
					temp.next = newNode;
					newNode.prev = temp;
					newNode.next = ptr;
					ptr.prev = newNode;
					found = true;
				} else {
					temp = ptr;
					ptr = temp.next;
				}
			}
			if (!found) {
				temp.next = newNode;
				newNode.prev = temp;
			}
		}
		size++;

	}
	/**
	 * Udskriver elementerne fra listen i sorteret rækkefølge
	 */
	public void udskrivElements() {
		if (size > 0) {
			Node temp = first;
			while (temp != last) {

				System.out.print(temp.data + " ");
				temp = temp.next;

			}
		}
	}

	/**
	 * Returnerer antallet af elementer i listen
	 */
	public int countElements() {
		return size;
	}
	/**
	 * Fjerner det sidste (altså det største) element i listen. Listen skal fortsat være sorteret i henhold til den
	 * naturlige ordning på elementerne.
	 *
	 * @return true der blev fjernet et element blev fjernet ellers returneres false.
	 */
	public boolean removeLast() {
		boolean removed = false;
		if (first != null) {
			if (first.next == null) {
				first = null;
			} else {
				Node temp = first;
				while (temp.next.next != null) {
					temp = temp.next;
				}
				System.out.println("Removed last element: " + temp.next.data);
				temp.next = null;
			}
			removed = true;
		}
		return removed;
	}
	/**
	 * Fjerner den første forekomst af e i listen. Listen skal fortsat være sorteret i henhold til den naturlige ordning
	 * på elementerne.
	 *
	 * @return true hvis e blev fjernet fra listen ellers returneres false.
	 */
	public boolean removeElement(String e) {
		boolean removed = false;
		if (first != null) {
			if (first.data.equals(e)) {
				first = first.next;
				removed = true;
			} else {
				Node temp = first;
				while (temp.next != null) {
					if (temp.next.data.equals(e)) {
						temp.next = temp.next.next;
						removed = true;
					} else {
						temp = temp.next;
					}
				}
			}
		}
		System.out.println("Removed: " + e);
		return removed;
	}

	class Node {
		public String data;
		public Node next;
		public Node prev;
	}

}
