package linkedlist;

public class SortedLinkedList {
	private Node first;
	public SortedLinkedList() {
		first = null;
	}

	/**
	 * Tilføjer e til listen, så listen fortsat er sorteret i henhold til den naturlige ordning på elementerne
	 */
	public void addElement(String e) {

		Node newNode = new Node();
		newNode.data = e;
		if (first == null) {
			first = newNode;
		} else if (e.compareTo(first.data) <= 0) {
			newNode.next = first;
			first = newNode;
		} else {
			Node temp = first;
			boolean found = false;
			while (!found && temp.next != null) {
				if (temp.next.data.compareTo(newNode.data) >= 0) {
					found = true;
					newNode.next = temp.next;
					temp.next = newNode;
				} else {
					temp = temp.next;
				}
			}
			if (!found) {
				temp.next = newNode;
			}
		}

	}
	/**
	 * Udskriver elementerne fra listen i sorteret rækkefølge
	 */
	public void udskrivElements() {
		if (first != null) {
			boolean finished = false;
			Node temp = first;
			while (!finished) {
				if (temp.next != null) {
					System.out.print(temp.data + ", ");
					temp = temp.next;
				} else {
					System.out.print(temp.data);
					finished = true;
				}

			}
		}
	}

	/**
	 * Returnerer antallet af elementer i listen
	 */
	public int countElements() {
		int count = 0;
		if (first != null) {
			count++;
			Node temp = first;
			while (temp.next != null) {
				count++;
				temp = temp.next;
			}
		}
		return count;
	}
	/**
	 * Fjerner det sidste (altså det største) element i listen. Listen skal fortsat være sorteret i henhold til den
	 * naturlige ordning på elementerne.
	 *
	 * @return true der blev fjernet et element blev fjernet ellers returneres false.
	 */
	public boolean removeLast() {
		boolean removed = false;
		if (first != null) {
			if (first.next == null) {
				first = null;
			} else {
				Node temp = first;
				while (temp.next.next != null) {
					temp = temp.next;
				}
				System.out.println("Removed last element: " + temp.next.data);
				temp.next = null;
			}
			removed = true;
		}
		return removed;
	}
	/**
	 * Fjerner den første forekomst af e i listen. Listen skal fortsat være sorteret i henhold til den naturlige ordning
	 * på elementerne.
	 *
	 * @return true hvis e blev fjernet fra listen ellers returneres false.
	 */
	public boolean removeElement(String e) {
		boolean removed = false;
		if (first != null) {
			if (first.data.equals(e)) {
				first = first.next;
				removed = true;
			} else {
				Node temp = first;
				while (temp.next != null) {
					if (temp.next.data.equals(e)) {
						temp.next = temp.next.next;
						removed = true;
					} else {
						temp = temp.next;
					}
				}
			}
		}
		System.out.println("Removed: " + e);
		return removed;
	}
	/**
	 * Tilføjer alle elementerne fra list til den aktuelle liste. Listen er fortsat sorteret i henhold til den naturlige
	 * ordning på elementerne.
	 */
	public void addAll(SortedLinkedList list) {
		if (list.first != null) {
			Node temp = list.first;
			if (first != null) {
				while (temp.next != null) {
					addElement(temp.data);
					temp = temp.next;
				}
				addElement(temp.data);
			} else {
				first = temp;
				Node set = first;
				while (temp.next != null) {
					set.next = temp.next;
					set = set.next;
					temp = temp.next;
				}

			}
		}
	}

	class Node {
		public String data;
		public Node next;
	}
}