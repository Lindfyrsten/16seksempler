package eksempel;

import java.util.Scanner;

/**
 * @author Kristian Lindbjerg
 */
public class Eksempel {
    
    /*
     * Static initializing bruges når vi skal sætte nogle værdier eller kalde metoder fra starten. Det bliver
     * initialiseret før alt andet i klasssen inklusiv evt constructor
     */
    private static boolean flag = initialize();
    private static int B, H;
    
    private static boolean initialize() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Indtast bredde værdi");
        B = scan.nextInt();
        System.out.println("Indtast højde værdi");
        H = scan.nextInt();
        scan.close();
        if (B <= 0 || H <= 0) {
            System.out.println("java.lang.Exception: Breadth and height must be positive");
            return false;
        }
        else {
            return true;
        }
    }

    public static void main(String[] args) {
        if (flag) {
            int area = B * H;
            System.out.println("Areal: " + area);
        }
    }
}
