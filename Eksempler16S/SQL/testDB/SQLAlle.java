package testDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLAlle {

    public static void main(String[] args) {
        try {
            Connection minConnection;
            minConnection = DriverManager.getConnection(
                "jdbc:sqlserver://PCNAVN;databaseName=DATABASE;user=USER;password=PASSWORD;");
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE); // opretter statement object
            ResultSet res = stmt.executeQuery("select regNr, navn, adresse, tlfNr from Afdeling");
            while (res.next()) {
                System.out.println(res.getString(1) + "    " + res.getString(2) + "   "
                    + res.getString(3) + "   " + res.getString(4));
            }
// res.previous();
// res.previous();
// System.out.println(res.getString(1) + " " + res.getString(2));
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (minConnection != null) {
                minConnection.close();
            }
        }
        catch (Exception e) {
            System.out.println("fejl:  " + e.getMessage());
        }
    }
}
