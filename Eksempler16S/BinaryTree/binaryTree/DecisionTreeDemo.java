package binaryTree;

/**
 * This program demonstrates a decision tree for an animal guessing game.
 */
public class DecisionTreeDemo {
    
    public static void main(String[] args) {
        BinaryTree<Integer> t25 = new BinaryTree<>(25);
        BinaryTree<Integer> t15 = new BinaryTree<>(15);
        BinaryTree<Integer> t30 = new BinaryTree<>(30, t25, null);
        BinaryTree<Integer> t11 = new BinaryTree<>(11, t15, null);
        BinaryTree<Integer> t88 = new BinaryTree<>(88);
        BinaryTree<Integer> t90 = new BinaryTree<>(90, t88, null);
        BinaryTree<Integer> t77 = new BinaryTree<>(77, t90, null);
        BinaryTree<Integer> t22 = new BinaryTree<>(22, t11, t30);
        BinaryTree<Integer> root = new BinaryTree<>(45, t22, t77);
        System.out.println("Size: " + root.size());
        System.out.println("Height: " + root.height());
        System.out.println("Count: " + root.count());
// root.preorder();
// root.postorder();
// root.inorder();
// BinaryTree<String> tiger = new BinaryTree<>("It ia a tiger.");
// BinaryTree<String> zebra = new BinaryTree<>("It is a zebra.");
// BinaryTree<String> canivore = new BinaryTree<>("Is it a carnivore?", tiger, zebra);
// BinaryTree<String> pig = new BinaryTree<>("It is a pig.");
// BinaryTree<String> stripes = new BinaryTree<>("Does it have stripes?", canivore, pig);
// BinaryTree<String> penguin = new BinaryTree<>("It is a penguin.");
// BinaryTree<String> ostrich = new BinaryTree<>("It is an ostrich.");
// BinaryTree<String> swim = new BinaryTree<>("Does it swim?", penguin, ostrich);
// BinaryTree<String> eagle = new BinaryTree<>("It is an eagle.");
// BinaryTree<String> fly = new BinaryTree<>("Does it fly?", eagle, swim);
// BinaryTree<String> questionTree = new BinaryTree<>("Is it a mammal?", stripes, fly);
// System.out.println(questionTree.size());
// questionTree.inorder();
// boolean done = false;
// Scanner in = new Scanner(System.in);
// while (!done) {
// BinaryTree<String> left = questionTree.left();
// BinaryTree<String> right = questionTree.right();
// if (left.isEmpty() && right.isEmpty()) {
// System.out.println(questionTree.data());
// done = true;
// }
// else {
// String response;
// do {
// System.out.print(questionTree.data() + " (Y/N) ");
// response = in.next().toUpperCase();
// } while (!response.equals("Y") && !response.equals("N"));
// if (response.equals("Y")) {
// questionTree = left;
// }
// else {
// questionTree = right;
// }
// }
// }
// in.close();
    }
}
