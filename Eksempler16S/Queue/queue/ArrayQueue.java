package queue;

/**
 * An implementation of a queue as a array.
 */
public class ArrayQueue implements QueueI {
	private Object[] elements;
	private int tail = 0;

	/**
	 * Constructs an empty queue.
	 */
	public ArrayQueue() {
		elements = new Object[10];
	}

	/**
	 * Checks whether this queue is empty.
	 *
	 * @return true if this queue is empty
	 */
	@Override
	public boolean isEmpty() {
		return tail == 0;
	}

	/**
	 * Adds an element to the tail of this queue.
	 *
	 * @param newElement
	 *            the element to add
	 */
	@Override
	public void enqueue(Object newElement) {
		elements[tail] = newElement;
		tail++;
		growIfNecessary();
	}

	/**
	 * Removes an element from the head of this queue.
	 *
	 * @return the removed element
	 */
	@Override
	public Object dequeue() {
		if (tail == 0) {
			throw new NullPointerException("Listen er tom");
		} else {
			tail--;
			return elements[tail + 1] = null;
		}

	}

	/**
	 * Returns the head of this queue. The queue is unchanged.
	 *
	 * @return the head element
	 */
	@Override
	public Object getFront() {
		if (tail == 0) {
			throw new NullPointerException("Listen er tom");
		} else {
			return elements[0];
		}

	}

	/**
	 * The number of elements on the queue.
	 *
	 * @return the number of elements in the queue
	 */
	@Override
	public int size() {
		return tail;
	}
	/**
	 * Grows the element array if the current size equals the capacity.
	 */
	private void growIfNecessary() {
		if (tail == elements.length) {
			Object[] newElements = new Object[2 * elements.length];
			for (int i = 0; i < elements.length; i++) {
				newElements[i] = elements[i];
			}
			elements = newElements;
		}
	}
}
