package queue;

import java.util.NoSuchElementException;

/**
 * An implementation of a queue as a circular array.
 */
public class CircularArrayQueue implements QueueI, DequeI {
	private Object[] elements;
	private int currentSize;
	private int head;
	private int tail;

	/**
	 * Constructs an empty queue.
	 */
	public CircularArrayQueue() {
		final int INITIAL_SIZE = 10;
		elements = new Object[INITIAL_SIZE];
		currentSize = 0;
		head = 0;
		tail = 0;
	}

	/**
	 * Checks whether this queue is empty.
	 *
	 * @return true if this queue is empty
	 */
	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}

	/**
	 * Adds an element to the tail of this queue.
	 *
	 * @param newElement
	 *            the element to add
	 */
	@Override
	public void enqueue(Object newElement) {
		growIfNecessary();
		currentSize++;
		elements[tail] = newElement;
		tail = (tail + 1) % elements.length;
	}

	/**
	 * Removes an element from the head of this queue.
	 *
	 * @return the removed element
	 */
	@Override
	public Object dequeue() {
		if (currentSize == 0) {
			throw new NoSuchElementException();
		}
		Object removed = elements[head];
		head = (head + 1) % elements.length;
		currentSize--;
		return removed;
	}

	/**
	 * Grows the element array if the current size equals the capacity.
	 */
	private void growIfNecessary() {
		if (currentSize == elements.length) {
			Object[] newElements = new Object[2 * elements.length];
			for (int i = 0; i < elements.length; i++) {
				newElements[i] = elements[(head + i) % elements.length];
			}
			elements = newElements;
			head = 0;
			tail = currentSize;
		}
	}

	@Override
	public Object getFront() {
		if (currentSize == 0) {
			throw new NoSuchElementException();
		}
		return elements[head];
	}

	@Override
	public int size() {
		return currentSize;
	}

	@Override
	public void addFirst(Object newElement) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addLast(Object newElement) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object removeFirst() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object removeLast() {
		Object temp = null;
		if (currentSize != 0) {
			int i = elements.length;
			boolean removed = false;
			while (!removed && i > 0) {
				if (elements[i] != null) {
					temp = elements[i];
					elements[i] = null;
					removed = true;
				} else {
					i--;
				}
			}
		}
		return temp;
	}

	@Override
	public Object getFirst() {
		if (currentSize == 0) {
			throw new NoSuchElementException();
		}
		return elements[head];
	}

	@Override
	public Object getLast() {
		if (currentSize == 0) {
			throw new NoSuchElementException();
		}
		return elements[tail];
	}
}
