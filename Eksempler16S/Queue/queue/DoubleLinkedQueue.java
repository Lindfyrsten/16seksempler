package queue;
public class DoubleLinkedQueue implements DequeI {

	private Node first;
	private Node last;

	public DoubleLinkedQueue() {
		first = null;
		last = null;
	}

	@Override
	public boolean isEmpty() {
		return first == null;
	}

	@Override
	public void addFirst(Object newElement) {
		Node newNode = new Node();
		newNode.data = newElement;
		if (first == null) {
			last = newNode;
		} else {
			newNode.next = first;
			first.prev = newNode;
		}
		first = newNode;
	}

	@Override
	public void addLast(Object newElement) {
		Node newNode = new Node();
		newNode.data = newElement;
		if (first == null) {
			first = newNode;
		} else {
			newNode.prev = last;
			last.next = newNode;
		}
		last = newNode;
	}

	@Override
	public Object removeFirst() {
		Node temp = null;
		if (first != null) {
			temp = first;
			if (first == last) {
				first = null;
				last = null;
			} else {
				first = first.next;
				first.prev = null;
			}
		}
		return temp.data;
	}

	@Override
	public Object removeLast() {
		Node temp = null;
		if (last != null) {
			temp = last;
			if (first == last) {
				first = null;
				last = null;
			} else {
				last = last.prev;
				last.next = null;
			}
		}
		return temp.data;
	}

	@Override
	public Object getFirst() {
		return first.data;
	}

	@Override
	public Object getLast() {
		return last.data;
	}

	@Override
	public int size() {
		int count = 0;
		if (first != null) {
			count++;
			Node temp = first;
			while (temp != last) {
				count++;
				temp = temp.next;
			}
		}
		return count;
	}
	class Node {

		public Object data;
		public Node next;
		public Node prev;
	}
}
