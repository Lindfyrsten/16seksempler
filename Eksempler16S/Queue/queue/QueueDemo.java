package queue;

public class QueueDemo {
	public static void main(String[] args) {
		// QueueI q = new ArrayQueue();
		// QueueI q = new LinkedListQueue();
		// q.enqueue("Tom");
		// q.enqueue("Diana");
		// q.enqueue("Harry");
		// q.enqueue("Thomas");
		// q.enqueue("Bob");
		// q.enqueue("Brian");
		// ((LinkedListQueue) q).udskrivElements();
		// System.out.println("\nRemoved " + q.dequeue());
		// System.out.println("Front : " + q.getFront());
		// System.out.println("Empty: " + q.isEmpty() + "\nSize: " + q.size());
		// while (!q.isEmpty()) {
		// System.out.println("Removed " + q.dequeue());
		// }
		// ((LinkedListQueue) q).udskrivElements();
		// System.out.println();
		// System.out.println("Empty: " + q.isEmpty() + "\nSize: " + q.size());

		// LinkedDoubleQueue
		DequeI q = new DoubleLinkedQueue();
		q.addFirst("Tom");
		q.addFirst("Harry");
		q.addLast("Thomas");
		q.addFirst("Bob");
		q.addFirst("Diana");
		q.addFirst("Brian");
		System.out.println(q.size());
		System.out.println(q.removeFirst());
		System.out.println(q.removeLast());
		System.out.println(q.size());
		System.out.println(q.removeFirst());
		System.out.println(q.removeFirst());
		System.out.println(q.removeFirst());
		System.out.println(q.removeFirst());
		System.out.println(q.size());
	}
}
